<?php
$translations = array(
    'comments' => 'Kommentare',

    'name' => 'Name',
    'email' => 'E-Mail',
    'comment' => 'Kommentar',

    'emailHint' => 'E-Mail-Adresse wird nicht veröffentlicht. Gravatar wird unterstützt.',

    'submit' => 'Abschicken',
    'reply' => 'Antworten',
    'writeComment' => 'Ein Kommentar schreiben...',

    'noCommentsYet' => 'Noch keine Kommentare. Sei der erste!',
    'successMessage' => 'Kommentar erfolgreich abgesendet.',
    'failMessage' => 'Kommentar konnte nicht gesendet werden. Grund: ',

    'dateString' => 'vor {}',
    'years' => 'Jahren',
    'months' => 'Monaten',
    'days' => 'Tagen',
    'hours' => 'Stunden',
    'minutes' => 'Minuten',
    'seconds' => 'Sekunden',

    # reply notification e-mail
    'subject' => 'Antwort auf dein Kommentar von {}',
    'introduction' => 'es gibt eine Antwort auf dein Kommentar.',
    'author' => 'Autor',
    'message' => 'Nachricht',
    'unsubscribeDescription' => 'Um solche Benachrichtigungen nicht mehr zu erhalten, klicke auf folgenden Link:',
);