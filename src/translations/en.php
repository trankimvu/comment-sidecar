<?php
$translations = array(
    'comments' => 'Comments',

    'name' => 'Name',
    'email' => 'E-Mail',
    'comment' => 'Comment',

    'emailHint' => 'E-Mail will not be published. Gravatar is supported.',

    'submit' => 'Submit',
    'reply' => 'Reply',
    'writeComment' => 'Write a Comment...',

    'noCommentsYet' => 'No comments yet. Be the first!',
    'successMessage' => 'Successfully submitted comment.',
    'failMessage' => "Couldn't submit your comment. Reason: ",

    'dateString' => '{} ago',
    'years' => 'years',
    'months' => 'months',
    'days' => 'days',
    'hours' => 'hours',
    'minutes' => 'minutes',
    'seconds' => 'seconds',

    # reply notification e-mail
    'subject' => 'Reply to your comment by {}',
    'introduction' => 'there is a reply to our comment.',
    'author' => 'Author',
    'message' => 'Message',
    'unsubscribeDescription' => 'To unsubscribe mail notifications like this please click on the following link:',
);